<?php
/**
 * get name controller
 */
if ( ! function_exists('name_controller'))
{
	/**
	 * @return string
	 */
	function name_controller()
	{
	  list($controller, $acao) = explode('.', Route::currentRouteName());

	  return $controller;
	}
}

/**
 * get current route
 */
if ( ! function_exists('get_current_route'))
{
	/**
	 * @return string
	 */
	function get_current_route()
	{
		return Route::current()->getAction()['controller'];
	}
}

/**
 * get controller slug
 */
if ( ! function_exists('get_controller_slug'))
{
	/**
	 * @param  string $separator
	 * @return string
	 */
	function get_controller_slug($separator = '_')
	{
		return \Str::snake(str_replace('Controller', '', get_current_route()), $separator);
	}
}

/**
 * Debug with code
 */
if ( ! function_exists('de'))
{
	/**
	 * Debug style
	 * @return string
	 */
	function de()
	{
	  array_map(function($x){
	  	dump($x);
	    // echo "<pre>";
	    //   print_r($x);
	    // echo "</pre>";
	  }, func_get_args());
	  die;
	}
}

if ( ! function_exists('action_args'))
{
  /**
   * Generate a URL to a controller action.
   *
   * @param  string  $name
   * @param  array   $parameters
   * @return string
   */
  function action_args($name, $parameters = array())
  {
    $parameters = array_merge($parameters, Input::all());

    return app('url')->action($name, $parameters);
  }
}

/**
 * Color Hexidecimal randomica
 */
if ( ! function_exists('random_color'))
{
  function random_color() {
    $letters = '0123456789ABCDEF';
    $color = '#';
    for($i = 0; $i < 6; $i++) {
      $index = rand(0,15);
      $color .= $letters[$index];
    }

    return $color;
  }
}

/**
 * Formata int para real padrão Brasil
 */
if ( ! function_exists('real'))
{
  function real($number=0)
  {
    if (is_numeric($number)) {
      return number_format($number, 2, ',', '');
    }
  }
}

/**
 * Retorna a URL sem mostrar erro caso não exista
 * a action
 */
if ( ! function_exists('get_action'))
{
  function get_action($action='index')
  {
    list($controller, $action) = explode('@', $action);
    if (method_exists(new $controller, $action)) {
      return action($controller . '@' . $action);
    }

    return action($controller . '@' . Meta::getAction());
  }
}

/**
 * Função que formata uma data de acordo com o padrão
 * especificado.
 * Um exemplo é o padrão brasileiro de datas dia/mes/ano (padrão PT-BR)
 */
if ( ! function_exists('format_date'))
{
  /**
   * Formata datas.
   *
   * @todo Caso a data não seja válida não será exibida
   * @param  string $date   data do banco, sistema de arquivo ou so.
   * @param  string $format formato aceito pelo linguagem
   * @return string
   */
  function format_date($date, $format='d/m/Y')
  {
    $date = str_replace('/', '-', $date);
    $timestamp = strtotime($date);
    if ($timestamp !== false)
    {
      return date($format, strtotime($date));
    }
  }
}

/**
 * Esta função remove caracteres especiais e deixa somente numeros
 */
if ( ! function_exists('str_number'))
{
  /**
   * Remove caracteres e permite numeros
   * @param  string $value
   * @return string
   */
  function str_number($value)
  {
    return preg_replace("/[^0-9\s]/", "", $value);
  }
}

if ( ! function_exists('db_float'))
{
  /**
  * Converte para float nivel banco
  */
  function db_float($value)
  {
      if (str_contains($value, ','))
      {
        $value = str_replace('.', '', $value);
        $value = str_replace(',', '.', $value);
      }

      return $value;
  }
}

if ( ! function_exists('array_equalize'))
{
  /**
  * Linear valores em array
  */
  function array_equalize(array $list, array $values)
  {
  
    $dados = [];
    if (count($list))
    {
      foreach ($list as $k => $v)
      {
        if (isset($values[$k])) {
          # new array
          $dados[$k] = $list[$k] . ' ' . $values[$k];
          unset($list[$k]);
          unset($values[$k]);
        }
      }
    }

    return array_merge($list, $dados, $values);
  }
}
