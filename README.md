# BR Support #

Esta é uma biblioteca composta de helpers e utilitários comuns usados pela BR Software.
### Instalação ###
No arquivo **composer.json** adicione essa linha em require.

```
#!json

"require": {
 "br/support": "dev-master"
},
"repositories": [
{
  "type": "vcs",
  "url":  "git@bitbucket.org:br_software/br-support.git"
}
```

### Funções do ROTA (Laravel 4.2) ###

* **name_controller** : retorna o nome do controller;
* **get_current_route** : rota atual;
* **get_controller_slug** : retorna o nome do controller sem a palavra Controller;

### Funções dependentes ###
* **action_args** : cria um link com os parâmetros passado pelo Input::all() dinamicamente

### Funções não dependentes ###
* de (aceita vários parâmetros) : var dump baseado no Symfony [Leia mais](http://symfony.com/doc/current/components/var_dumper/introduction.html)
* **random_color** : gera hexadecimal randômico
* **real** : formata um número para o formato brasileiro de moeda
* **format_date** : função que formata uma data de acordo com o padrão especificado;
  
```
#!php
<?php
  # retorna 12/03/2009
  echo format_date('2009-03-12', 'd/m/Y');
```

* **str_number** : apenas número e retornado
* **db_float** : formata tipo float para ficar compatível com qualquer SGBD
* **array_equalize** : deixa um array de chaves e um array de valores em forma linear